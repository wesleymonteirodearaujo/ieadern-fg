import { createStore } from 'vuex';
import axios from 'axios';
import membroService from '../services/membroService';
import membroEscaladoService from '../services/membroEscaladoService';


const api_URL = 'http://localhost:8000/api/';

export default createStore({
  state: {
    membros: [],
    eventos: [],
    escalas: [],
    escalados: [],
  },
  mutations: {
    SET_MEMBROS(state, membros) {
      state.membros = membros;
    },
    SET_EVENTOS(state, eventos) {
      state.eventos = eventos;
    },
    SET_ESCALAS(state, escalas) {
      state.escalas = escalas;
    },
    SET_ESCALADOS (state, escalados) {
      state.escalados = escalados;
    },
  },
  actions: {
    carregarMembros({ commit }) {
      return membroService.carregarMembros()
        .then(membros => {
          commit('SET_MEMBROS', membros);
        })
        .catch(error => {
          console.error('Erro ao carregar membros no Vuex:', error);
        });
    },
    carregarEscalados({ commit }) {
      return membroEscaladoService.carregarEscalados()
        .then(escalados => {
          commit('SET_ESCALADOS', escalados);
        })
        .catch(error => {
          console.error('Erro ao carregar membros no Vuex:', error);
        });
    },
    carregarEventos({ commit }) {
      axios.get(api_URL + 'evento')
        .then(response => {
          commit('SET_EVENTOS', response.data.data);
        })
        .catch(error => {
          console.error('Erro ao carregar events:', error);
        });
    },
    carregarEscalas({ commit }) {
      axios.get(api_URL + 'escala')
        .then(response => {
          commit('SET_ESCALAS', response.data.data);
        })
        .catch(error => {
          console.error('Erro ao carregar escalas:', error);
        });
    },

  },
  getters: {
    // getters, se necessário
  },
});
