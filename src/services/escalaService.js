// services/membroService.js
import axios from 'axios';

const api_URL = 'http://localhost:8000/api/';

export default {
    carregarEscala() {
        return axios.get(api_URL + 'escala')
        .then(response => response.data.data)
        .catch(error => {
          console.error('Erro ao carregar escala:', error);
          throw error; // Propague o erro para quem chamar esse serviço, se necessário
        });
    },
};









