// services/membroService.js
import axios from 'axios';

const api_URL = 'http://localhost:8000/api/';

export default {
  carregarMembros() {
    return axios.get(api_URL + 'membro')
      .then(response => response.data.data)
      .catch(error => {
        console.error('Erro ao carregar membros:', error);
        throw error; // Propague o erro para quem chamar esse serviço, se necessário
      });
  },
};
