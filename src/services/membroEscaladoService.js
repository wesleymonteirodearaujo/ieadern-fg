// services/membroService.js
import axios from 'axios';

const api_URL = 'http://localhost:8000/api/';

export default {

  carregarEscalados() {
    return axios.get(api_URL + 'escalado')
      .then(response => response.data.data)
      .catch(error => {
        console.error('Erro ao carregar eventos:', error);
        throw error; // Propague o erro para quem chamar esse serviço, se necessário
      });
  },
};
